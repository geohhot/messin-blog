#include <inttypes.h>
#include <avr/io.h>
#include <util/delay.h>

void init_timer1(void) {
  /* 
   * Timer configuration:
   * Fast pwm mode (8 bit)
   * Freq: (divider: 1)
   * Non inverting mode
   */
  TCCR1A = _BV(COM1A1)|_BV(COM1B1)|_BV(WGM10); //|_BV(WGM11);
  TCCR1B = _BV(WGM12)| /* _BV(CS12)| */_BV(CS10);
}

void enc_init (void) {
  // set input pins
  DDRD &= ~ ( _BV(PD2)|_BV(PD3) );
  // set inner pullups
  PORTD |= ( _BV(PD2)|_BV(PD3) );
  // ready to GO!
}

/* int main (void) { */
/*   init_timer1(); */
/*   DDRD |= _BV(4)|_BV(5); // set PWM pins as outputs */
/*   uint8_t ocr = 0; */
/*   while (1) { */
/*     ocr ++; */
/*     OCR1A = ocr; */
/*     _delay_ms (1); */
/*   } */
/*   return 0; */
/* } */

/* 
 * Returns gray's code:
 * 0 - none
 * 1 - only B
 * 2 - only A
 * 3 - both
 */
uint8_t get_enc_code (void) {
  if (PIND & _BV(PD2))
    // not A
    if (PIND & _BV(PD3))
      // none
      return 0;
    else
      // B
      return 1;
  else
    // A
    if (PIND & _BV(PD3))
      // only A
      return 2;
    else
      // both
      return 3;
}

int main (void) {
  init_timer1();
  enc_init();
  DDRD |= _BV(4)|_BV(5); // set PWM pins as outputs
  uint8_t ocr = 0, enc_last_code=0, enc_code;
  uint8_t enc_rotation = 0; // 0 can be our forever..
  // 0 - no rotation
  // 1 - cw
  // 2 - ccw
  while (1) {
    enc_code = get_enc_code();
    if (enc_code == 3) {
      // we got to point where both A and B are low
      // sombody turned the darn knob
      // now checking the last position...
      if (enc_last_code == 3)
	enc_rotation = 0; // this part is for debouncing
      else if (enc_last_code == 2) // from A -> both
	enc_rotation = 1;
      else if (enc_last_code == 1) // from B -> both
	enc_rotation = 2;
    } else
      enc_rotation = 0;

    enc_last_code = enc_code;
    
    if (enc_rotation == 1) ocr += 10;
    if (enc_rotation == 2) ocr -= 10;

    OCR1A = ocr;
    _delay_ms (2);
  }
  return 0;
}
