#include <inttypes.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <stdio.h>
#include <util/delay.h>
#include <math.h>

#include "uart_v2.h"
#include "i2c.h"

char uart_buff [40];
volatile uint8_t uart_buff_index = 0;

void init_timer0 (void) {
  /* 
   * 8 bit timer counter 0
   * Mode of operation: CTC
   * Divider: 1-64
   * Interrupt frequency: 1 ms
   */
  TCCR0 = _BV(WGM01) | _BV(CS01)|_BV(CS00);
  TIMSK |= _BV(OCIE0);
  OCR0 = 249;
}

ISR (TIMER0_COMP_vect) {
  
}

int __attribute__((noreturn)) main (void) {
  init_uart (TWO_BITS, NO_PAR, DATA_8, 25, INT_OFF);  // 38,4 k
  init_timer0();
  init_twi (15, 0);
  debug_out ("~=~=~=~=~=~=~=~=~=~=~=~");
  debug_out ("BMP 180 v0.2");
  debug_out ("Waiting for start ...");
  _delay_ms (10);
  
  sei();

  if (twi_start_tx (0xEE) != _SLA_W_ACK) {
    debug_out ("No device found, check connections .. :(");
    while (1);
  }

  /* Read debugged data from EEPROM of BMP180 */
  twi_send_data (0xAA);
  twi_start_tx (0xEF);
  twi_request_data (22);
  int16_t  AC1 = twi_read () << 8 | twi_read();
  int16_t  AC2 = twi_read () << 8 | twi_read();
  int16_t  AC3 = twi_read () << 8 | twi_read();
  uint16_t AC4 = twi_read () << 8 | twi_read();
  uint16_t AC5 = twi_read () << 8 | twi_read();
  uint16_t AC6 = twi_read () << 8 | twi_read();
  int16_t  B1  = twi_read () << 8 | twi_read();
  int16_t  B2  = twi_read () << 8 | twi_read();
  int16_t  MB  = twi_read () << 8 | twi_read();  
  int16_t  MC  = twi_read () << 8 | twi_read();  
  int16_t  MD  = twi_read () << 8 | twi_read();
  
  /* debug_out ("AC1: %d", AC1); */
  /* debug_out ("AC2: %d", AC2); */
  /* debug_out ("AC3: %d", AC3); */
  /* debug_out ("AC4: %u", AC4); */
  /* debug_out ("AC5: %u", AC5); */
  /* debug_out ("AC6: %u", AC6); */
  /* debug_out ("B1 : %d",  B1); */
  /* debug_out ("B2 : %d",  B2); */
  /* debug_out ("MB : %d",  MB); */
  /* debug_out ("MC : %d",  MC); */
  /* debug_out ("MD : %d",  MD); */
  twi_stop ();

  debug_out ("Got init, ready to go!");

  uint16_t UT = 0, UP = 0;
  long X1, X2, B5, T;
  long B6, X3, B3, p;
  unsigned long B4, B7;
  
  while (1) {
    /* Ask for temperature */
    twi_start_tx  (0xEE);
    twi_send_data (0xF4);
    twi_send_data (0x2E);
    twi_stop ();

    _delay_ms (5);
    /* Read the temperature */
    twi_start_tx  (0xEE);
    twi_send_data (0xF6);
    twi_start_tx  (0xEF);
    twi_request (2);
    UT = twi_read () << 8 | twi_read();
    twi_stop ();

    /* debug_out ("UT: %u", UT); */
    
    /* Ask for pressure */
    twi_start_tx  (0xEE);
    twi_send_data (0xF4);
    twi_send_data (0x34); // OSS = 0
    twi_stop ();

    _delay_ms (5);
    /* Read the pressure */
    twi_start_tx  (0xEE);
    twi_send_data (0xF6);
    twi_start_tx  (0xEF);
    twi_request (2);
    UP = twi_read() << 8 | twi_read();
    twi_stop ();

    /* debug_out ("UP: %u", UP); */

    /* Calculate true temperature */
    X1 = (UT - AC6) * 1.0 * AC5 / 32768;
    X2 = MC * 2048.0 / (X1 + MD);
    B5 = X1 + X2;
    T  = (B5 + 8) / 16;

    debug_out ("Temperature: %.1f %cC", T/10.0, (char)176);

    /* Calculate true pressure */
    B6 = B5 - 4000;
    X1 = (B2 * (B6 * B6 / 4096.0)) / 2048.0;
    X2 = AC2 * B6 / 2048.0;
    X3 = X1 + X2;
    B3 = ((AC1 + X3/4.0) + 0.5); /* Weird ? */
    X1 = AC3 * B6 / 8192.0;
    X2 = (B1 * (B6 * B6 / 4096.0)) / 262144.0;
    X3 = ((X1 + X2) + 2) / 4;
    B4 = AC4 * (unsigned long)(X3 + 32768) / 32768.0;
    B7 = ((unsigned long)UP - B3) * 50000;
    if (B7 < 0x80000000) { p = (B7 * 2.0) / B4; }
    else { p = (1.0 * B7 / B4) * 2.0; }
    X1 = (p / 256.0) * (p / 256.0);
    X1 = (X1 * 3038) / 65536.0;
    X2 = (-7375 * p) / 65536.0;
    p = p + (X1 + X2 + 3791) / 8;

    debug_out ("Pressure: %ld pa", p);

    /* Calculate altitude */
    /* double k = p / 101325.0; */
    /* double k1 = 1 / 5.255; */
    /* k = pow (k, 2); */
    debug_out ("Altitude: unkown");

    _delay_ms (500);
    while (1);
  }
}
