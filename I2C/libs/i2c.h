/*
 * DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *          Version 2, December 2004
 *
 * Copyright (C) 2014 Gevorg Hindoyan <ghindoyan@gmail.com>
 *
 * Everyone is permitted to copy and distribute verbatim or modified
 * copies of this license document, and changing it is allowed as long
 * as the name is changed.
 *
 *         DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 * TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
 *
 * 0. You just DO WHAT THE FUCK YOU WANT TO.
 */

/* 
 * I2C (TWI) library, designed for Atmega16
 * I'll try to keep it universal, tho
 * Version: pre alpha
 */

#ifndef _I2C_LIB_
#define _I2C_LIB_

#define _I2CCR_ TWCR
#define _I2CSR_ TWSR
#define _I2CBR_ TWBR
#define _I2CDR_ TWDR

// Macro definitions

#define _WAIT_FOR_TWI_BUS_ while (!(_I2CCR_ & _BV(TWINT)));
#define _I2COut_ (_I2CCR_ = _BV(TWEN) | _BV(TWINT))
#define _I2CStatus_ (_I2CSR_ & 0xF8)

#define twi_start() {					\
  _I2CCR_ = _BV(TWEN) | _BV(TWSTA) | _BV(TWINT);	\
}

#define twi_stop() {					\
  _I2CCR_ = _BV(TWEN) | _BV(TWSTO) | _BV(TWINT);	\
}

// Constant definitions
#define _START_TXed    0x08
#define _RESTART_TXed  0x10
#define _SLA_W_ACK     0x18
#define _SLA_W_NACK    0x20
#define _DATA_W_ACK    0x28
#define _DATA_W_NACK   0x30
#define _BUS_LOST      0x38

#define _SLA_R_ACK     0x40
#define _SLA_R_NACK    0x48
#define _DATA_R_ACK    0x50
#define _DATA_R_NACK   0x58

#define _TWI_NO_INFO   0xF8

/* Variable definitions */
static uint8_t _I2C_requested = 0;

/* Function definitions */

void init_twi (uint8_t baud, uint8_t prescaler) {
  _I2CBR_ = baud;
  _I2CCR_ |= prescaler & 0x03;
}

/* 
 *
 * Returns:
 * 1 - if the start ceremony dindn't get the ACK
 */
uint8_t twi_start_tx (uint8_t address) {
  twi_start();
  _WAIT_FOR_TWI_BUS_;
  if (_I2CStatus_ != _START_TXed &&		\
      _I2CStatus_ != _RESTART_TXed) return 1;

  _I2CDR_ = address;
  _I2COut_;
  _WAIT_FOR_TWI_BUS_;
  return _I2CStatus_;
}

/* 
 * Request given amount of bytes
 */
void twi_request_data (uint8_t count) {
  _I2C_requested = count;
}

#define twi_request twi_request_data

/* 
 * Read one byte of data
 */
uint8_t twi_read_data (void) {
  if (_I2C_requested == 0) return 0x00;
  -- _I2C_requested;

  if (_I2C_requested == 0)
    _I2COut_;
  else
    _I2CCR_ = _BV(TWEN) | _BV(TWINT) | _BV(TWEA);

  _WAIT_FOR_TWI_BUS_;
  return _I2CDR_;
}

#define twi_read twi_read_data

/* 
 *
 * Returns:
 * 0 - when everything is cool
 * 1 - when status error occurs
 */
uint8_t twi_read_datan (uint8_t* data, uint8_t size) {
  uint8_t register i = 0;
  for (i=0; i<size; ++i) {
    if (i == size - 1) { // the last byte
      _I2COut_;
    } else {
      _I2CCR_ = _BV(TWEN) | _BV(TWINT) | _BV(TWEA);
    }
    _WAIT_FOR_TWI_BUS_;
    if (_I2CStatus_ != _SLA_R_ACK && i != size -1		\
	&& _I2CStatus_ != _DATA_R_ACK) return 1;
    
    data[i] = _I2CDR_;
  }
  return 0;
}

/* 
 *
 * Returns:
 * 1 - error occurred, wrong status code
 */
uint8_t twi_send_data (uint8_t data) {
  _WAIT_FOR_TWI_BUS_;
  if (_I2CStatus_ != _SLA_W_ACK			\
      && _I2CStatus_ != _DATA_W_ACK) return 1;

  _I2CDR_ = data;
  _I2COut_;
  _WAIT_FOR_TWI_BUS_;
   return _I2CStatus_;
}

/* 
 * 
 * Returns:
 * 1 - error occurred, no Slave Address Ack or no Data write ack
 * 2 - finished writing, no errors ?
 */
uint8_t twi_send_datan (uint8_t* data, uint8_t size) {
  uint8_t register i = 0;
  for (i=0; i<size; ++i) {
    _WAIT_FOR_TWI_BUS_;
    if (_I2CStatus_ == _DATA_W_NACK) // finished writing
      return 2;
    if (_I2CStatus_ != _SLA_W_ACK		\
	&& _I2CStatus_ != _DATA_W_ACK) return 1;
    _I2CDR_ = data[i];
    _I2COut_;
  }
  return 0;
}

#endif
