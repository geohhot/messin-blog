/* 
 * Simple UART audio recorder .. kindof
 */

#include <inttypes.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <stdio.h>
#include <util/delay.h>
#include <avr/sleep.h>

#include "uart_v2.h"

void adc_init (uint8_t muxx) {
  SFIOR |= _BV(ADTS1) | _BV(ADTS0); // TIMER0 compare match
  ADMUX =  _BV(REFS0) |_BV(REFS1) | _BV(ADLAR)| muxx;
  ADCSRA = _BV(ADEN) | _BV(ADPS2) | _BV(ADIE) | _BV(ADATE)
    /* | _BV(ADPS1) */;
}

ISR (ADC_vect) {
  // send converted ADC data over uart
  UDR = ADCH;
  // clear TIMER0 compare match flag
  TIFR |= _BV(OCF0);
}

char uart_buff [40];
volatile uint8_t uart_buff_index = 0;

void init_timer0 (void) {
  /* 
   * 8 bit timer counter 0
   * Mode of operation: CTC
   * Divider: 8 / OCR - 199 (200 divider)
   * Interrupt frequency: 10000 hertz
   */
  TCCR0 = _BV(WGM01) | _BV(CS01);
  // TIMSK |= _BV(OCIE0);
  OCR0 = 199;
}

ISR (TIMER0_COMP_vect) {
  //ADCSRA |= _BV(ADSC);
  //UDR = ADCH;
}

int __attribute__((noreturn)) main (void) {
  // 1 Meg - we want to squeeze as much as possible 
  init_uart (TWO_BITS, NO_PAR, DATA_8, 0, INT_OFF); 
  init_timer0();
  //  adc_init (2); // PA2 - input
  adc_init (0b00001101); // PA2 - 10x

  DDRA |= _BV(PA0);

  sei();
  
  while (1) {
    // set_sleep_mode (SLEEP_MODE_ADC);
    /* PORTA ^= _BV(PA0); */
    /* _delay_ms (250); */
  }
}
