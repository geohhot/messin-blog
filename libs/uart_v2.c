/*
 * DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *          Version 2, December 2004
 *
 * Copyright (C) 2014 Gevorg Hindoyan <ghindoyan@gmail.com>
 *
 * Everyone is permitted to copy and distribute verbatim or modified
 * copies of this license document, and changing it is allowed as long
 * as the name is changed.
 *
 *         DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 * TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
 *
 * 0. You just DO WHAT THE FUCK YOU WANT TO.
 */

#include <avr/io.h>
#include <avr/interrupt.h>

#include <string.h>
#include <stdio.h>

#include "uart_v2.h"

#if defined uart_buff
extern char uart_buff [40];
extern volatile uint8_t uart_buff_index;
#else
char uart_buff [40];
volatile uint8_t uart_buff_index;
#endif

void start_transmission (void) {
  uart_buff_index = 0;
  //  UDR = uart_buff [uart_buff_index++];
  UDRIE_set;
  // UCSRB |= (1<<UDRIE);
}

void raw_send (void) {
  UDRIE_clear;
  uint8_t i = 0;
  for (i=0; i< strlen (uart_buff); ++i) {
    UDR = uart_buff[i];
    do {} while ((UCSRA & _BV (UDRE) ) == 0);
  }
}

void send_uint (uint8_t num) {
  sprintf (uart_buff, "%u\n\r", num);
  start_tx();
}

ISR (USART_UDRE_vect) {
  if (uart_buff_index < strlen (uart_buff))
    UDR = uart_buff [uart_buff_index++];
  else
    UDRIE_clear;
}

#ifndef UART_V2_NO_INTERRUPT

/* ISR (USART_TXC_vect) { */

/* } */

/* ISR (USART_RXC_vect) { */

/* } */

#endif

/*
 * init_uart - set baud and other shtuff that UART needs
 * TODO: comment out all this
 */
void init_uart (stop_bits_t stop, parity_bits_t parity,
		data_bits_t data_bus, uint16_t baud,
		interr_bits_t interrupts) {
  UBRRH = (unsigned char)(baud)>>8;
  UBRRL = (unsigned char)(baud);

  UCSRB = 0b11011000;
  UCSRC = 0b10000000;

  if (interrupts != INT_ON)
    UCSRB &= ~ (0b11000000);
  
  switch (stop) {
  case TWO_BITS:
    UCSRC |= (1<<URSEL)|(1<<USBS);
    break;
  default: break;
  }
  
  switch (parity) {
  case ODD:
    UCSRC |= (1<<URSEL)|(1<<UPM1)|(1<<UPM0);
    break;
  case EVEN:
    UCSRC |= (1<<URSEL)|(1<<UPM1);
    break;
  default: break;
  }

  switch (data_bus)  {
  case DATA_6:
    UCSRC |= (1<<7)|(1<<1);
    break;
  case DATA_7:
    UCSRC |= (1<<7)|(1<<2);
    break;
  case DATA_8:
    UCSRC |= (1<<7) | (1<<1) | (1<<2);
    break;
  case DATA_9:
    UCSRC |= (1<<7) | (1<<1) | (1<<2);
    UCSRB |= (1<<2);
    break;
  default: break;
  }
}
