/*
 * DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *          Version 2, December 2004
 *
 * Copyright (C) 2014 Gevorg Hindoyan <ghindoyan@gmail.com>
 *
 * Everyone is permitted to copy and distribute verbatim or modified
 * copies of this license document, and changing it is allowed as long
 * as the name is changed.
 *
 *         DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 * TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
 *
 * 0. You just DO WHAT THE FUCK YOU WANT TO.
 */

/* 
 * 1 wire library
 * Version 0.0.2
 * Usage: Just set _1_WIRE_PORT_L and _1_WIRE_PORT_N
 * Notice, you can set it either here, or in any local file
 */

#ifndef _1_WIRE_LIB_
#define _1_WIRE_LIB_

/* The place for your changes */
#ifndef _1_WIRE_PORT_L
#define _1_WIRE_PORT_L B
#define _1_WIRE_PORT_N 0
#endif
/* No needs to make any changes further away */

#ifndef _GLUE
#define _GLUE(x,y) x ## y
#define _EVAL(x,y)  _GLUE(x,y)
#endif

#define _1_WIRE_DDR  _EVAL (DDR,  _1_WIRE_PORT_L)
#define _1_WIRE_PIN  _EVAL (PIN,  _1_WIRE_PORT_L)
#define _1_WIRE_PORT _EVAL (PORT, _1_WIRE_PORT_L)

#define _1_WIRE_RX_1 (_1_WIRE_PIN & _BV(_1_WIRE_PORT_N))

#define _1_WIRE_OUT_LOW (_1_WIRE_DDR |= _BV(_1_WIRE_PORT_N))
#define _1_WIRE_OUT_HIZ (_1_WIRE_DDR &= ~_BV(_1_WIRE_PORT_N))

#define _1_WIRE_TX_1 { _delay_us (10); _1_WIRE_OUT_HIZ; _delay_us (50); }
#define _1_WIRE_TX_0 { _delay_us (60); _1_WIRE_OUT_HIZ; }

/* 
 * Not really necessary
 */
void one_wire_init (void) {
  _1_WIRE_OUT_HIZ;
  _1_WIRE_PORT &= ~_BV(_1_WIRE_PORT_N); // just to make sure it's zero
}

/* 
 * Returns 0 - when couldn't get presence pulse
 * Non 0     - when managed to get presence pulse
 */
uint8_t one_wire_reset (void) {
  _1_WIRE_OUT_LOW;
  _delay_us (480);
  _1_WIRE_OUT_HIZ;
  _delay_us (60);

  uint8_t rx = _1_WIRE_RX_1;
  _delay_us (240);
  if (rx)
    return 0;
  return 1;
}

/* 
 * Transmits one byte of data over 1 wire interface
 */

/* extern char uart_buff[40]; */

void one_wire_write (uint8_t data) {
  /* debug_out ("Wrote: 0x%02X", data); */
  register uint8_t i = 0;
  for (i=0; i<8; i++) {
    _1_WIRE_OUT_LOW;
    if (data & _BV(i)) {
      _1_WIRE_TX_1;
    }
    else {
      _1_WIRE_TX_0;
    }
    _delay_us (1);
  }
}

/* 
 * Returns one byte read from 1 wire interface
 */
uint8_t one_wire_read (void) {
  register uint8_t i = 0;
  uint8_t rx = 0;
  for (i=0; i<8; i++) {
    _1_WIRE_OUT_LOW;
    _delay_us (2);
    _1_WIRE_OUT_HIZ;

    _delay_us (15);
    if (_1_WIRE_RX_1)
      rx |= _BV(i);

    _delay_us (45);
  }

  _delay_us (1);
  /* debug_out ("Read: 0x%02X", rx); */
  return rx;
}

/* 
 * Receives given amount of bytes from 1 wire interface
 */
void one_wire_receive (uint8_t *array, uint8_t count) {
  register uint8_t i = 0;
  for (i=0; i < count; ++i)
    array[i] = one_wire_read ();
}

/* 
 * Send given amount of bytes from array "data"
 */
void one_wire_send_data (uint8_t *data, uint8_t count) {
  register uint8_t i = 0;
  for (i=0; i < count; ++i)
    one_wire_write (*(data+i));
}

/* 
 * Sends a whole package - Reset/Presence, ROM command, Function command
 * Returns 0 - if no presence
 */
uint8_t one_wire_send_package (uint8_t rom_command,
			       uint8_t *rom_args, uint8_t rom_arg_count,
			       uint8_t function_command,
			       uint8_t *func_args, uint8_t func_argsc) {
  if (one_wire_reset ()) {
    one_wire_write (rom_command);

    if (rom_args != NULL) 
      one_wire_send_data (rom_args, rom_arg_count);
    
    if (function_command != 0) {
      one_wire_write (function_command);
      if (func_args != NULL)
	one_wire_send_data (func_args, func_argsc);
    }
    return 1;
  }
  return 0;
}

/* 
 * Sends only ROM command and Function Command
 */
uint8_t one_wire_send (uint8_t rom_command, uint8_t function_command) {
  if (one_wire_reset()) {
    one_wire_write (rom_command);
    if (function_command != 0)
      one_wire_write (function_command);
    return 1;
  }
  return 0;
}

#endif
