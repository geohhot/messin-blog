/*
 * DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *          Version 2, December 2004
 *
 * Copyright (C) 2014 Gevorg Hindoyan <ghindoyan@gmail.com>
 *
 * Everyone is permitted to copy and distribute verbatim or modified
 * copies of this license document, and changing it is allowed as long
 * as the name is changed.
 *
 *         DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 * TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
 *
 * 0. You just DO WHAT THE FUCK YOU WANT TO.
 */

#ifndef _UART_LIB_
#define _UART_LIB_

#include <inttypes.h>
#include <stdarg.h>

typedef enum { ONE_BIT, TWO_BITS } stop_bits_t;
typedef enum { NO_PAR, ODD, EVEN } parity_bits_t;
typedef enum {DATA_5, DATA_6,
	      DATA_7, DATA_8, DATA_9} data_bits_t;
typedef enum { INT_ON, INT_OFF } interr_bits_t;

/* ******************** */
/*       FUNCTIONS      */
/* ******************** */

void init_uart (stop_bits_t stop, parity_bits_t parity,
		data_bits_t data_bus, uint16_t baud,
	        interr_bits_t interrupts);

void start_transmission (void);
#define start_tx start_transmission

void raw_send (void);
#define send_raw raw_send

void send_uint (uint8_t);

/* 
 * Sends the contents of uart_buff, 
 * waiting for transmission to finish
 */
#define send_rw(format_str, ...) {			\
    sprintf (uart_buff, format_str, ##__VA_ARGS__);	\
    send_raw();						\
  }

/* 
 * Sends the contents of uart_buff, char by char,
 * using interrupts
 * (keep in mind, global interrupts must be enabled
 * before using this function)
 */
#define send(format_str, ...) {				\
    sprintf (uart_buff, format_str, ##__VA_ARGS__);	\
    start_tx();						\
  }

/* 
 * Clears the terminal screen
 */
#define send_clr() { UDR = '\f'; }

/* 
 * Outputs the debug information including:
 * file name, line number, and given info
 */

/* #define debug_out(format_str, ...) {					\ */
/*   sprintf								\ */
/*   (uart_buff, "[%s:%d] " format_str "\n\r", __FILE__, __LINE__, ##__VA_ARGS__); \ */
/*   send_raw();								\ */
/* } */

#define debug_out(format_str, ...) {					\
  sprintf (uart_buff, "[%s:%d] ",  __FILE__, __LINE__);		        \
  send_raw();								\
  sprintf (uart_buff, format_str "\n\r", ##__VA_ARGS__);		\
  send_raw();								\
}

/*
 * Macros that can be used
 */

#define UDRIE_set   (UCSRB |=  (1<<UDRIE))
#define UDRIE_clear (UCSRB &= ~(1<<UDRIE))

#endif
