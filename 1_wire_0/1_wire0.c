#include <inttypes.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <stdio.h>
#include <util/delay.h>

#include "uart_v2.h"

char uart_buff [40];
volatile uint8_t uart_buff_index = 0;

void init_timer0 (void) {
  /* 
   * 8 bit timer counter 0
   * Mode of operation: CTC
   * Divider: 1-64
   * Interrupt frequency: 1 ms
   */
  TCCR0 = _BV(WGM01) | _BV(CS01)|_BV(CS00);
  TIMSK |= (1<<OCIE0);
  OCR0 = 249;
}

ISR (TIMER0_COMP_vect) {
  
}

void reset_presence (void) {
  // send reset / presence pulses
  DDRB |= _BV(PB0); // make it output, keep it low
  PORTB &= ~_BV(PB0);
  _delay_us (480); // wait for some time
  DDRB &= ~_BV(PB0); // make it into input again
  _delay_us (60);

  if (! (PINB & _BV(PB0)) ) {
    PORTA ^= _BV(PA0);
    send_rw (">>> Got presence pulse ...\n\r");
  } else
    send_rw (">>> No response, check connections \n\r");
}

void write_1 (void) {
  // pull the bus low
  DDRB |= _BV(PB0);
  // wait within 15 us
  _delay_us (10);
  // go hi-z
  DDRB &= ~_BV(PB0);
  // wait to finish waiting 60 us
  _delay_us (50);
}

void write_0 (void) {
  // pull the bus low
  DDRB |= _BV(PB0);
  // wait 60 us
  _delay_us (60);
  // go hi-z
  DDRB &= ~_BV(PB0);
}

void write (uint8_t data) {
  // lsb first
  register uint8_t i = 0;
  for (i=0; i<8; ++i) {
    if (data & _BV(i)) write_1();
    else write_0();
    _delay_us (1);
  }
}

uint8_t read (void) {
  uint8_t rx = 0;
  register uint8_t i=0;
  for (i=0; i<8; i++) {
    // pull the bus low
    DDRB |= _BV(PB0);
    // wait 1 us
    _delay_us (1);
    // go hi-z
    DDRB &= ~_BV(PB0);

    _delay_us (10);

    if (PINB & _BV(PB0)) {
      // rx 1
      rx |= _BV(i);
    } else {
      // rx 0
      rx &= ~_BV(i);
    }

    _delay_us (50);
  }
  
  _delay_us (2);
  return rx;
}

/* const */ uint8_t family_code = 0x20,				\
  CRC_byte = 0xff,						\
  calculated_CRC = 0x00,					\
  serial_code[6] = {0xe0, 0xe0, 0x20, 0x20, 0xff, 0xe0};
uint8_t i;

uint8_t calc_CRC (void) {
  // CRC = X ^ 8 + X ^ 5 + X ^ 4 + 1
  return 0;
}

int main (void) {
  init_uart (TWO_BITS, NO_PAR, DATA_8, 25, INT_OFF);  // 38,4 k
  init_timer0();

  send_rw ("\f==Starting==\n\r");

  // PA0 - heartbeat
  DDRA |= _BV(PA0);
  // PB0 - 1 wire interface

  // send reset / presence pulses
  // if the LED lights up, then success
  reset_presence ();
  
  // ROM commands
  write (0x33); // read rom (64 bit)

  family_code = read ();
  debug_out ("FC: 0x%02X", family_code);
  for (i=0; i<6; ++i) {
    serial_code[i] = read();
    debug_out ("Serial code [%u]: 0x%02X", i, serial_code[i]);
  }
  CRC_byte = read();
  debug_out ("CRC byte: 0x%02x", CRC_byte);
  
  calculated_CRC = calc_CRC ();
  debug_out ("Our CRC : 0x%02x", calculated_CRC);

  /* 
   * Datasheet says:
   * After Search ROM command
   * The restart / presence step should be repeated 
   */

  // send reset / presence pulses .. again
  reset_presence ();

  // send match ROM (0x55) command
  debug_out ("Sending MATCH ROM (0x55) command..");
  write (0x55);
  // send ROM 64 bit data
  write (family_code);
  for (i=0; i<6; ++i)
    write (serial_code[i]);
  write (CRC_byte);

  /* // send SKIP rom (0xCC) command */
  /* debug_out ("Sending SKIP ROM (0xCC) command.."); */
  /* write (0xCC); */

  // send convert t (0x44) command
  debug_out ("Sending Convert T (0x44) command..");
  write (0x44);

  while (read() == 0) _delay_us (1);
  debug_out ("Finished converting temperature..");

  // send reset / presence pulses .. again
  reset_presence ();

  /* // send SKIP rom (0xCC) command */
  /* debug_out ("Sending SKIP ROM (0xCC) command.."); */
  /* write (0xCC); */
  
  // send match ROM (0x55) command
  debug_out ("Sending MATCH ROM (0x55) command..");
  write (0x55);
  // send ROM 64 bit data
  write (family_code);
  for (i=0; i<6; ++i)
    write (serial_code[i]);
  write (CRC_byte);
  
  // send READ SCRATCHPAD (0xBE)
  debug_out ("Sending READ SCRATCHPAD (0xBE) command..");
  write (0xBE);

  uint8_t scratchpad[8];
  for (i=0; i<9; ++i) {
    scratchpad[i] = read();
    debug_out ("Scratchpad[%u] = 0x%02x", i, scratchpad[i]);
  }
  debug_out ("Finished READ SCRATCHPAD (0xBE).");

  double temperature = 0;
  uint8_t temp = 0;
  temp |= (scratchpad[1]&0x03);
  temp <<= 4;
  temp |= ((scratchpad[0]&0xF0)>>4);

  temperature = temp;
  for (i=0; i<4; i++)
    if (scratchpad[0] & _BV(i))
      temperature += 1.0/(1<<(4-i));
  
  if (scratchpad[1] & 0xF0) temperature *= -1;

  send_rw (">>> Calculated temperature: %.4f (c)", temperature);
  
  sei();
  
  while (1) {
    
  }
  return 0;
}
