#include <inttypes.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <stdio.h>
#include <util/delay.h>

#include <util/crc16.h>

#define _1_WIRE_PORT_L A
#define _1_WIRE_PORT_N 5

#include "uart_v2.h"
#include "1_wire.h"

char uart_buff [40];
volatile uint8_t uart_buff_index = 0;

void init_timer0 (void) {
  /* 
   * 8 bit timer counter 0
   * Mode of operation: CTC
   * Divider: 1-64
   * Interrupt frequency: 1 ms
   */
  TCCR0 = _BV(WGM01) | _BV(CS01)|_BV(CS00);
  TIMSK |= (1<<OCIE0);
  OCR0 = 249;
}

ISR (TIMER0_COMP_vect) {
  
}

uint8_t ROM[8], scratchpad[9];
int main (void) {
  init_uart (TWO_BITS, NO_PAR, DATA_8, 25, INT_OFF);  // 38,4 k
  send_clr();
  debug_out ("1 wire library test #2");
  init_timer0();
  one_wire_init();

  DDRA |= _BV(PA0);
  
  sei();
  uint8_t i;

  debug_out ("Resetting ROM");
  for (i=0; i<8; i++)
    ROM[i] = 0xFF;
  
  if (one_wire_send (0x33, 0))
    debug_out ("Sen't READ ROM (0x33)"); //read rom
  one_wire_receive (ROM, 8);

  for (i=0; i<8; ++i)
    debug_out ("ROM [%u] = 0x%02X", i, ROM[i]);

  while (1) {
      
    debug_out ("Asking for Convert T");
    if (one_wire_send_package (0x55, ROM, 8,  0x44, NULL, 0)) {
      debug_out ("Got reply, started converting..");
    }
    else {
      debug_out ("Err: no presence !");
    }
    
    /* if (one_wire_reset()) */
    /*   debug_out ("Got presence .."); */
    /* one_wire_write (0x55); */
    /* for (i=0; i<8; i++) */
    /*   one_wire_write (ROM[i]); */
    /* one_wire_write (0x44); */
    
    while (one_wire_read() == 0);

    debug_out ("Got temperature. ");
    debug_out ("Asking for Scratchpad (0xBE)");
    // one_wire_send (0xCC, 0xBE);
    one_wire_send_package (0x55, ROM, 8, 0xBE, NULL, 0);

    one_wire_receive (scratchpad, 9);

    uint8_t CRC = 0;
    for (i=0; i<8; ++i) {
      CRC = _crc_ibutton_update (CRC, scratchpad[i]);
    }

    if (CRC == scratchpad[8]) {
      debug_out ("CRC match!");
    }
    else {
      debug_out ("CRC mismatch :(");
    }
    
    /* debug_out ("Got scratchpad, data: "); */
    /* for (i=0; i<9; i++) */
    /*   debug_out ("Scratchpad [%u] = 0x%02X", i, scratchpad[i]); */

    debug_out ("Scratchpad (0,1) = (0x%02X, 0x%02X)",
	       scratchpad[0], scratchpad[1]);

    /* 
       [1_wire2.c:84] Scratchpad (0,1) = (0x26, 0x01)
       [1_wire2.c:99] Afterall temperature: 18.1250
       [1_wire2.c:100] Temp in float      : 18.375000
    */

    int16_t temp_int = (scratchpad[1]<<4)|(scratchpad[0]>>4);
    uint16_t temp_float = 625 * ( (scratchpad[0]&_BV(3))*8 + 
				  (scratchpad[0]&_BV(2))*4 + 
				  (scratchpad[0]&_BV(1))*2 + 
				  (scratchpad[0]&_BV(0)) );
    temp_float /= 10;
    if (scratchpad[1] & 0xF0) temp_int *= -1;

    debug_out ("Afterall temperature: %d.%04u", temp_int, temp_float);

    _delay_ms (2000);
    PORTA ^= _BV(PA0);
  }
  return 0;
}
